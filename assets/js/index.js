$(document).ready(function() {
    $('.login').load('views/login.html');
    $('.wrapper').hide();
    $('.footer').hide();

    $(document).on('click', 'input:button', (function() {
        var $email = $.trim($("#email").val());
        var $pass = $.trim($("#password").val());

        //Users
        //coordinadora@umariana.edu.co
        //rectora@umariana.edu.co
        //vicerectora@umariana.edu.co
        //dirprogram@umariana.edu.co
        //estudiante@umariana.edu.co

        if ($email === "coordinadora@umariana.edu.co" && $pass === "12345") {
            $('.login').hide();
            $('.wrapper').show();
            $('.footer').show();
            $('#main-panel').load('views/agreements.html');
            $('.sidebar').load('views/base/sidebar-coord.html');
            $('#perfil').val('Coordinadora');
            //alert($email + ' - ' + $pass)
        } else if ($email === "rectora@umariana.edu.co" && $pass === "12345") {
            $('.login').hide();
            $('.wrapper').show();
            $('.footer').show();
            $('#main-panel').load('views/mobilities.html');
            $('.sidebar').load('views/base/sidebar-rec.html');
            $('#perfil').val('Rectora');
            //alert($email + ' - ' + $pass)
        } else if ($email === "vicerectora@umariana.edu.co" && $pass === "12345") {
            $('.login').hide();
            $('.wrapper').show();
            $('.footer').show();
            $('#main-panel').load('views/mobilities-cant.html');
            $('.sidebar').load('views/base/sidebar-vice.html');
            $('#perfil').val('Vicerrectora');
            //alert($email + ' - ' + $pass)
        } else if ($email === "dirprogram@umariana.edu.co" && $pass === "12345") {
            $('.login').hide();
            $('.wrapper').show();
            $('.footer').show();
            $('#main-panel').load('views/events.html');
            $('.sidebar').load('views/base/sidebar-dprog.html');
            $('#perfil').val('Director de Programa');
            //alert($email + ' - ' + $pass)
        } else if ($email === "estudiante@umariana.edu.co" && $pass === "12345") {
            $('.login').hide();
            $('.wrapper').show();
            $('.footer').show();
            $('#main-panel').load('views/view-agreements.html');
            $('.sidebar').load('views/base/sidebar-est.html');
            $('#perfil').val('Estudiante');
            //alert($email + ' - ' + $pass)
        }else{
            alert("usuario o contraseña incorrectos.");
        }
        document.getElementById("frm-sesion").reset();
    }));

    $(document).on('click', 'a', (function() {
        var $temp = $.trim($(this).text());
        //alert($.trim($temp));

        //Opciones Generales
        if ($temp == "Salir") {
            $('.login').show();
            $('.wrapper').hide();
            $('.footer').hide();

            //Menú Coordinadora
        } else if ($temp == "Registrar Convenios") {
            $('#main-panel').load('views/agreements.html');
            $("li").removeClass("active");
            $("#agreements").addClass("active");
        } else if ($temp == "Registrar Movilidad") {
            $('#main-panel').load('views/mobility.html');
            $("li").removeClass("active");
            $("#mobility").addClass("active");
        } else if ($temp == "Registrar Docente") {
            $('#main-panel').load('views/doc-external.html');
            $("li").removeClass("active");
            $("#doc-external").addClass("active");
            
            //Menú Rectora
        } else if ($temp == "Ver movilidades") {
            $('#main-panel').load('views/mobilities.html');
            $("li").removeClass("active");
            $("#movilities").addClass("active");
            //Filtrado table
            $("#tableSearch").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        } else if ($temp == "Ver docentes") {
            $('#main-panel').load('views/docs-external.html');
            $("li").removeClass("active");
            $("#docs-external").addClass("active");

            //Menú Vicerrectora
        } else if ($temp == "Cantidad Movilidades") {
            $('#main-panel').load('views/mobilities-cant.html');
            $("li").removeClass("active");
            $("#mobilities-cant").addClass("active");
        } else if ($temp == "Costos de Movilidad") {
            $('#main-panel').load('views/mobilities-cost.html');
            $("li").removeClass("active");
            $("#mobilities-cost").addClass("active");
        
            //Menú Director de Programa
        } else if ($temp == "Registrar Evento") {
            $('#main-panel').load('views/events.html');
            $("li").removeClass("active");
            $("#events").addClass("active");
        } else if ($temp == "Estudiantes Externos") {
            $('#main-panel').load('views/stu-external.html');
            $("li").removeClass("active");
            $("#stu-external").addClass("active");

            //Menú Estudiante
        } else if ($temp == "Ver Convenios") {
            $('#main-panel').load('views/view-agreements.html');
            $("li").removeClass("active");
            $("#view-agreements").addClass("active");
        } else if ($temp == "Aplicar Movilidad") {
            $('#main-panel').load('views/apply-mobility.html');
            $("li").removeClass("active");
            $("#apply-mobility").addClass("active");
        } 
    
    }));

    $(document).on('click', '#statistics',
        function() {
            // Javascript method's body can be found in assets/js/demos.js
            //alert("hello"); 

        }
    );

    //Select
    $(document.body).on('change', "#selectAgreements", function(e) {
        //doStuff
        var optVal = $("#selectAgreements option:selected").val();
        if (optVal == 1) {
            $('#lblSite').text('Pais');
            $('#contentImage').hide();
        } else if (optVal == 2) {
            $('#lblSite').text('Ciudad');
            $('#contentImage').show();
        }
    });    
});